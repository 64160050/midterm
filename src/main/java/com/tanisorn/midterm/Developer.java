package com.tanisorn.midterm;

public class Developer extends Employee {

    public Developer(String name, double salary) {
        super(name, salary);
        System.out.println("I'm Developer");
        System.out.println("-------------------------------");
    }
}
