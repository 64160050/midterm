package com.tanisorn.midterm;

public class Salesperson extends Employee {

    public Salesperson(String name, double salary){
        super(name,salary);
        System.out.println("I'm Salesperson");
        System.out.println("-------------------------------");
    }
}
