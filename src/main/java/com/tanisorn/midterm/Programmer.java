package com.tanisorn.midterm;

public class Programmer extends Employee {
    
    public Programmer(String name, double salary) {
        super(name,salary);
        System.out.println("I'm Programmer");
        System.out.println("-------------------------------");
    }
}
